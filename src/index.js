require("dotenv").config();
const path = require("path");
const express = require("express");

const app = express();

// Settings
app.set("port", process.env.PORT || 3100);

// Middlewares
if (process.env.NODE_ENV === "development") {
  app.use(require("morgan")("dev"));
}
app.use(express.static(path.join(__dirname, "/public")));

// Starting
const server = app.listen(app.get("port"), () => {
  console.log(`Server listening on port ${app.get("port")}`);
});

// Web socket

const SocketIO = require("socket.io");
const io = SocketIO(server);

io.on("connect", socket => {
  console.log("new connection: ", socket.id);

  socket.on("chat:message", data => {
    io.sockets.emit("chat:message", data);
  });

  socket.on("chat:typing", data => {
    socket.broadcast.emit("chat:typing", data);
  });
});
